program console_tests;

{$mode objfpc}{$H+}

uses {$IFDEF UNIX} {$IFDEF UseCThreads}
  cthreads, {$ENDIF} {$ENDIF}
  Classes,
  SysUtils,
  lazUTF8,
  fractions;

var
  Fr: array [1..3] of TFraction;
  i: longint;
  s: string;

  procedure WriteLnC(s: string);
  begin
    WriteLn(Utf8ToConsole(s));
  end;

begin

  WriteLnC('Тесты модуля работы с простыми дробями');
  WriteLnC('(с) java73, апрель 2017');
  try
    WriteLnC('Присваивание целочисленное');
    WriteLnC('Введите целое число:');
    ReadLn(i);
    Fr[3] := i;
    WriteLnC('Получилась дробь ' + Utf8ToConsole(Fr[3].toStr));
    WriteLnC('Присваивание строковое');
    for i := 1 to 2 do
    begin
      WriteLnC('Введите дробь ' + IntToStr(i) +
        ', разделив числитель и знаменатель символом / без пробелов:');
      ReadLn(s);
      Fr[i] := Trim(s);
      WriteLnC('Получилась дробь ' + Utf8ToConsole(Fr[i].toStr));
    end;
    // операции с дробями
    WriteLnC(Fr[1].toStr + ' + ' + Fr[2].toStr + ' = ' + (Fr[1] + Fr[2]).toStr);
    WriteLnC(Fr[1].toStr + ' - ' + Fr[2].toStr + ' = ' + (Fr[1] - Fr[2]).toStr);
    WriteLnC(Fr[1].toStr + ' * ' + Fr[2].toStr + ' = ' + (Fr[1] * Fr[2]).toStr);
    WriteLnC(Fr[1].toStr + ' / ' + Fr[2].toStr + ' = ' + (Fr[1] / Fr[2]).toStr);
    // смешанные операции
    i := Fr[3].Numerator;
    WriteLnC(Fr[1].toStr + ' + ' + IntToStr(i) + ' = ' + (Fr[1] + i).toStr);
    WriteLnC(Fr[1].toStr + ' - ' + IntToStr(i) + ' = ' + (Fr[1] - i).toStr);
    WriteLnC(Fr[1].toStr + ' * ' + IntToStr(i) + ' = ' + (Fr[1] * i).toStr);
    WriteLnC(Fr[1].toStr + ' / ' + IntToStr(i) + ' = ' + (Fr[1] / i).toStr);
    // неравенства
    WriteLnC(Fr[1].toStr + ' > ' + Fr[2].toStr+' '+BoolToStr(Fr[1]>Fr[2],true));
    WriteLnC(Fr[1].toStr + ' < ' + Fr[2].toStr+' '+BoolToStr(Fr[1]<Fr[2],true));
    WriteLnC(Fr[1].toStr + ' = ' + Fr[2].toStr+' '+BoolToStr(Fr[1]=Fr[2],true));
    WriteLnC(Fr[1].toStr + ' > 2/3 '+BoolToStr(Fr[1]>'2/3',true));
    WriteLnC(Fr[2].toStr + ' < 1 '+BoolToStr(Fr[2]<1,true));
  except
    on E: Exception do
    begin
      WriteLnC(E.Message);
      ReadLn(s);
    end;
  end;
  ReadLn(s);
end.



unit fractions;

{$mode objfpc}{$H+}
{$MODESWITCH ADVANCEDRECORDS}

interface

uses
  Classes, SysUtils, StrUtils;

const
  toGCD = 0;
  SolidorSym = '/';

type

  { TFraction }

  TFraction = record
    Numerator: longint;
    Denumerator: longint;
    function Create(ANum, ADenum: longint): TFraction;
    function toStr: string;
    function toFloat: extended;
  end;

  TFrCompareResult = (crLeft, crRight, crEqual);

procedure SetEqualDenum(var ALeftFr, ARightFr: TFraction);
function ExpandFraction(AFraction: TFraction; Factor: longint): TFraction;
function gcd(ALeftDenum, ARightDenum: longint): longint;
function lcm(ALeftDenum, ARightDenum: longint): longint;
function CollapseFraction(AFraction: TFraction; Divider: longint = toGCD): TFraction;
function CompareFractions(ALeftFr, ARightFr: TFraction): TfrCompareResult;
function ReverseFraction(AFraction: TFraction): TFraction;
operator +(ALeftFr, ARightFr: TFraction) r: TFraction;
operator +(ALeftFr: TFraction; const Term: longint) r: TFraction;
operator -(ALeftFr, ARightFr: TFraction) r: TFraction;
operator -(ALeftFr: TFraction; const Sub: longint) r: TFraction;
operator * (ALeftFr, ARightFr: TFraction) r: TFraction;
operator * (AFraction: TFraction; const Multiplier: longint) r: TFraction;
operator * (const Multiplier: longint; AFraction: TFraction) r: TFraction;
operator / (ALeftFr, ARightFr: TFraction) r: TFraction;
operator / (AFraction: TFraction; const Divider: longint) r: TFraction;
operator = (ALeftFr, ARightFr: TFraction) r: boolean;
operator > (ALeftFr, ARightFr: TFraction) r: boolean;
operator < (ALeftFr, ARightFr: TFraction) r: boolean;
operator := (const AIntegerPart: longint) r: TFraction;
operator := (const AStringFr: string) r: TFraction;

implementation

function ExpandFraction(AFraction: TFraction; Factor: longint): TFraction;
begin
  Result.Numerator := AFraction.Numerator * Factor;
  Result.Denumerator := AFraction.Denumerator * Factor;
end;

function gcd(ALeftDenum, ARightDenum: longint): longint;
begin
  if ARightDenum = 0 then
    Result := abs(ALeftDenum)
  else
    Result := abs(gcd(ARightDenum, ALeftDenum mod ARightDenum));
end;

function lcm(ALeftDenum, ARightDenum: longint): longint;
begin
  Result := abs(ALeftDenum * ARightDenum) div gcd(ALeftDenum, ARightDenum);
end;

function CollapseFraction(AFraction: TFraction; Divider: longint): TFraction;
var
  tDiv: longint;
begin
  if Divider = toGCD then
    tDiv := gcd(AFraction.Numerator, AFraction.Denumerator)
  else
    tDiv := Divider;
  Result.Numerator := AFraction.Numerator div tDiv;
  Result.Denumerator := AFraction.Denumerator div tDiv;
end;

procedure SetEqualDenum(var ALeftFr, ARightFr: TFraction);
var
  tDenum: longint;
begin
  if ALeftFr.Denumerator = ARightFr.Denumerator then
    exit;
  tDenum := lcm(ALeftFr.Denumerator, ARightFr.Denumerator);
  ALeftFr := ExpandFraction(ALeftFr, tDenum div ALeftFr.Denumerator);
  ARightFr := ExpandFraction(ARightFr, tDenum div ARightFr.Denumerator);
end;

function CompareFractions(ALeftFr, ARightFr: TFraction): TfrCompareResult;
begin
  SetEqualDenum(ALeftFr, ARightFr);
  if ALeftFr.Numerator > ARightFr.Numerator then
    Result := crLeft
  else
    Result := crRight;
  if ALeftFr.Numerator = ARightFr.Numerator then
    Result := crEqual;
end;

function ReverseFraction(AFraction: TFraction): TFraction;
begin
  Result.Numerator := AFraction.Denumerator;
  Result.Denumerator := AFraction.Numerator;
end;

operator+(ALeftFr, ARightFr: TFraction)r: TFraction;
begin
  SetEqualDenum(ALeftFr, ARightFr);
  r.Numerator := ALeftFr.Numerator + ARightFr.Numerator;
  r.Denumerator := ALeftFr.Denumerator;
  r := CollapseFraction(r, toGCD);
end;

operator+(ALeftFr: TFraction; const Term: longint)r: TFraction;
var
  TempFr: TFraction;
begin
  TempFr := Term;
  r := ALeftFr + TempFr;
end;

operator-(ALeftFr, ARightFr: TFraction)r: TFraction;
begin
  SetEqualDenum(ALeftFr, ARightFr);
  r.Numerator := ALeftFr.Numerator - ARightFr.Numerator;
  r.Denumerator := ALeftFr.Denumerator;
  r := CollapseFraction(r, toGCD);
end;

operator-(ALeftFr: TFraction; const Sub: longint)r: TFraction;
var
  TempFr: TFraction;
begin
  tempFr := Sub;
  r := ALeftFr - tempFr;
end;

operator*(ALeftFr, ARightFr: TFraction)r: TFraction;
begin
  r.Numerator := ALeftFr.Numerator * ARightFr.Numerator;
  r.Denumerator := ALeftFr.Denumerator * ARightFr.Denumerator;
  r := CollapseFraction(r, toGCD);
end;

operator*(AFraction: TFraction; const Multiplier: longint)r: TFraction;
var
  tempF: TFraction;
begin
  tempF := Multiplier;
  r := AFraction * tempF;
end;

operator*(const Multiplier: longint; AFraction: TFraction)r: TFraction;
begin
  r := AFraction * Multiplier;
end;

operator/(ALeftFr, ARightFr: TFraction)r: TFraction;
begin
  r := ALeftFr * ReverseFraction(ARightFr);
end;

operator/(AFraction: TFraction; const Divider: longint)r: TFraction;
var
  tempFr: TFraction;
begin
  tempFr := Divider;
  r := AFraction / tempFr;
end;

operator=(ALeftFr, ARightFr: TFraction)r: boolean;
begin
  Result := CompareFractions(ALeftFr, ARightFr) = crEqual;
end;

operator>(ALeftFr, ARightFr: TFraction)r: boolean;
begin
  Result := CompareFractions(ALeftFr, ARightFr) = crLeft;
end;

operator<(ALeftFr, ARightFr: TFraction)r: boolean;
begin
  Result := CompareFractions(ALeftFr, ARightFr) = crRight;
end;

operator:=(const AIntegerPart: longint)r: TFraction;
begin
  r.Numerator := AIntegerPart;
  r.Denumerator := 1;
end;

operator:=(const AStringFr: string)r: TFraction;
var
  i: integer;
begin
  i := PosEx(char(SolidorSym), AStringFr);
  if not TryStrToInt(LeftStr(AStringFr, i - 1), r.Numerator) then
    raise Exception.Create('Numerator is not integer!');
  if not TryStrToInt(RightStr(AStringFr, Length(AStringFr) - i), r.Denumerator) then
    raise Exception.Create('Denumerator is not integer!');
end;

{ TFraction }

function TFraction.Create(ANum, ADenum: longint): TFraction;
begin
  Result.Numerator := ANum;
  Result.Denumerator := ADenum;
end;

function TFraction.toStr: string;
begin
  Result := IntToStr(Numerator) + SolidorSym + IntToStr(Denumerator);
end;

function TFraction.toFloat: extended;
begin
  Result := Numerator / Denumerator;
end;

end.

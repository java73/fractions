unit main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Buttons, fractions;

type

  { TForm1 }

  TForm1 = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    gcdLabel: TLabel;
    CompareLabel: TLabel;
    toStr: TLabel;
    lcmLabel: TLabel;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
  private
    fResultFr: TFraction;
    LeftFr, RightFr: TFraction;
    procedure ReadFractions;
    procedure SetResultFr(AValue: TFraction);
  public
    property ResultFr: TFraction read fResultFr write SetResultFr;
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  ReadFractions;
  ResultFr := LeftFr + RightFr;
end;

procedure TForm1.BitBtn2Click(Sender: TObject);
begin
  ReadFractions;
  ResultFr := LeftFr - RightFr;
end;

procedure TForm1.BitBtn3Click(Sender: TObject);
begin
  ReadFractions;
  ResultFr := LeftFr * RightFr;
end;

procedure TForm1.BitBtn4Click(Sender: TObject);
begin
  ReadFractions;
  ResultFr := LeftFr / RightFr;
end;

procedure TForm1.ReadFractions;
begin
  LeftFr := TFraction.Create(StrToInt(Edit1.Text), StrToInt(Edit2.Text));
  RightFr := TFraction.Create(StrToInt(Edit3.Text), StrToInt(Edit4.Text));
end;

procedure TForm1.SetResultFr(AValue: TFraction);
var
  s: char;
begin
  fResultFr := AValue;
  Edit5.Text := IntToStr(fResultFr.Numerator);
  Edit6.Text := IntToStr(fResultFr.Denumerator);
  gcdLabel.Caption := 'НОД = ' + IntToStr(gcd(LeftFr.Denumerator, RightFr.Denumerator));
  lcmLabel.Caption := 'НОК = ' + IntToStr(lcm(LeftFr.Denumerator, RightFr.Denumerator));
  if LeftFr > RightFr then
    s := '>'
  else
    s := '<';
  if LeftFr = RightFr then
    s := '=';
  CompareLabel.Caption := 'дробь 1 ' + s + ' дробь 2';
  toStr.Caption := AValue.toStr;
end;

end.
